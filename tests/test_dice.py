"""EWC Commons Dice Tests

Test the happy & unhappy paths of creating and using the module.

TODO Test the unhappy path of supplying garbage to the dice.roll_die function.
TODO Test the object copy, repr of Dice & DiceShaker.
TODO Test the unhappy path for creating a DiceShaker with invalid Dice.
TODO Test the unhappy path for adding invalid Dice to a DiceShaker.
TODO Test getting a Dice from the DiceShaker.
"""
# Import pytest for testing & the dice module to test
import pytest
from ewccommons import dice


# TODO Test the unhappy path of supplying garbage to the function
@pytest.mark.parametrize(
    "_die_faces, _expected_max",
    [
        ([*range(1, 3 + 1)], 3),
        ([*range(1, 4 + 1)], 4),
        ([*range(1, 6 + 1)], 6),
        ([*range(1, 8 + 1)], 8),
        ([*range(1, 10 + 1)], 10),
        ([*range(1, 12 + 1)], 12),
        ([*range(1, 20 + 1)], 20),
    ],
)
def test_dice_rolls(_die_faces, _expected_max):
    """Happy Test

    Check rolling different length die faces do what they should.

    :param _die_faces: The list of dice face side values.
    :type _die_faces: `ewccommons.dice._Die_Faces_`
    :param _expected_max: The expected max number the dice face sides can roll.
    :type _expected_max: int
    """
    # Roll the die faces to get the roll value
    roll = dice.roll_die(_die_faces)
    # Verify it's an int
    assert isinstance(roll, int)
    # Verify it's within the bounds of 1 and the expected max
    assert roll >= 1
    assert roll <= _expected_max


@pytest.mark.parametrize(
    "_sides, _val, _expected_max",
    [
        (3, 1, 3),
        (4, 1, 4),
        (6, 1, 6),
        (8, 1, 8),
        (10, 1, 10),
        (12, 1, 12),
        (20, 1, 20),
    ],
)
def test_new_dice(_sides, _val, _expected_max):
    """Happy Test

    Check creating different sized dice objects is correct.

    :param _sides: The number of dice side faces.
    :type _sides: int
    :param _val: The starting rolled value of the dice.
    :type _val: int
    :param _expected_max: The expected max number the dice can roll.
    :type _expected_max: int
    """
    # Set the dice name to use
    _d_name = f"New D{_sides} Dice"
    # Create the new dice object
    _dice = dice.Dice(_sides, name=_d_name, val=_val)
    # Verify it's an Dice
    assert isinstance(_dice, dice.Dice)
    # Verify the dice equals what we think it should
    # - The string starting with the dice name
    # - The rolled value is in the string
    # - The length of the object is the number of face sides
    # - The numeric rolled value
    assert str(_dice).startswith(_d_name)
    assert str(_val) in str(_dice)
    assert len(_dice) == _sides
    assert _dice == _val
    # TODO Move this to it's own test for dice.roll()
    # Verify it's within the bounds of 1 and the expected max
    assert _dice >= 1
    assert _dice <= _expected_max


@pytest.mark.parametrize(
    "_invalid_sides",
    [
        (-1),
        (0),
        (1),
        (True),
        (False),
        ("hmmm"),
    ],
)
def test_new_dice_invalid_sides(_invalid_sides):
    """Unhappy Test

    Check that invalid Dice can not be created with invalid side faces.

    :param _invalid_sides: An invalid value for the number of dice side faces.
    :type _invalid_sides: Any
    """
    with pytest.raises(ValueError):
        # Try to create an invalid dice object
        dice.Dice(_invalid_sides)


@pytest.mark.parametrize(
    "_sides, _invalid_val",
    [
        (6, 7),
        (6, -1),
        (6, 0),
        (6, False),
        (6, "hmmm"),
    ],
)
def test_new_dice_invalid_val(_sides, _invalid_val):
    """Unhappy Test

    Check that invalid Dice can not be created with invalid starting rolled value.

    :param _sides: The number of dice side faces.
    :type _sides: int
    :param invalid_val: An invalid value for the starting rolled value of the dice.
    :type invalid_val: Any
    """
    with pytest.raises(ValueError):
        # Try to create an invalid dice object
        dice.Dice(_sides, name="Invalid die sides value", val=_invalid_val)


def test_new_empty_dice_shaker():
    """Happy Test

    Check creating an empty DiceShaker is what it is.
    """
    # Create the new empty shaker to test
    shaker = dice.DiceShaker()
    # Make sure it's an instance of what it is
    assert isinstance(shaker, dice.DiceShaker)
    # Make sure it's empty/blank when created empty
    assert len(shaker) == 0
    assert shaker == 0
    assert shaker.min_roll == 0
    assert shaker.max_roll == 0


def test_empty_dice_shaker():
    """Happy Test

    Check emptying a DiceShaker does what it does correctly.
    """
    # Create the new empty shaker to test
    shaker = dice.DiceShaker()
    # Empty the shaker, even though it's already empty lol
    shaker.empty_shaker()
    # TODO Reduce this test code duplication with tearUp & tearDown sort of stuff
    # Make sure it's empty/blank when created empty
    assert len(shaker) == 0
    assert shaker == 0
    assert shaker.min_roll == 0
    assert shaker.max_roll == 0


@pytest.mark.parametrize(
    "_dice_sides, _expected_min, _expected_max",
    [
        (3, 1, 3),
        (4, 1, 4),
        (6, 1, 6),
        (8, 1, 8),
        (10, 1, 10),
        (12, 1, 12),
        (20, 1, 20),
    ],
)
def test_new_dice_shaker(_dice_sides, _expected_min, _expected_max):
    """Happy Test

    Check creating an DiceShaker with Dice is what it is.

    :param _dice_sides: The number of dice side faces to create a Dice.
    :type _dice_sides: int
    :param _expected_min: The expected min number the shaker can roll.
    :type _expected_min: int
    :param _expected_max: The expected max number the shaker can roll.
    :type _expected_max: int
    """
    # Create a new Dn sided Dice to use with the shaker
    _dice = dice.Dice(_dice_sides)
    # Create the new shaker with 1 dice to test
    shaker = dice.DiceShaker([_dice])
    # Make sure it's an instance of what it is
    assert isinstance(shaker, dice.DiceShaker)
    # Make sure the shaker only contains 1 dice
    assert len(shaker) == 1
    # Make sure the shaker equals the rolled value of the 1 dice added
    assert shaker.roll_total == _dice.rolled
    # Make sure the min/max available roll values are correct
    assert shaker.min_roll == _expected_min
    assert shaker.max_roll == _expected_max


@pytest.mark.parametrize(
    "_dice_sides, _add, _expected_min, _expected_max",
    [
        (3, 4, 5, 15),
        (4, 4, 5, 20),
        (6, 4, 5, 30),
        (8, 4, 5, 40),
        (10, 4, 5, 50),
        (12, 4, 5, 60),
        (20, 4, 5, 100),
    ],
)
def test_dice_shaker_add(_dice_sides, _add, _expected_min, _expected_max):
    """Happy Test

    Check creating an DiceShaker with Dice is what it is.

    :param _dice_sides: The number of dice side faces to create a Dice.
    :type _dice_sides: int
    :param _add: The number of Dice to add to the shaker.
    :type _add: int
    :param _expected_min: The expected min number the shaker can roll.
    :type _expected_min: int
    :param _expected_max: The expected max number the shaker can roll.
    :type _expected_max: int
    """
    # Create a new Dn sided Dice to use with the shaker
    _dice = dice.Dice(_dice_sides)
    # Create the new shaker with 1 dice to test
    shaker = dice.DiceShaker([_dice])
    # Add the extra dice
    shaker.add(_dice, _add)
    # Make sure the shaker contains all the dice
    assert len(shaker) == _add + 1
    # Make sure the shaker equals the rolled value of the 1 dice * added + 1
    assert shaker.roll_total == (_dice.rolled * (_add + 1))
    # Make sure the min/max available roll values are correct
    assert shaker.min_roll == _expected_min
    assert shaker.max_roll == _expected_max


@pytest.mark.parametrize(
    "_dice_sides, _add, _expected_min, _expected_max",
    [
        (3, 5, 5, 15),
        (4, 5, 5, 20),
        (6, 5, 5, 30),
        (8, 5, 5, 40),
        (10, 5, 5, 50),
        (12, 5, 5, 60),
        (20, 5, 5, 100),
    ],
)
def test_dice_shaker_roll(_dice_sides, _add, _expected_min, _expected_max):
    """Happy Test

    Check creating an DiceShaker with Dice is what it is.

    :param _dice_sides: The number of dice side faces to create a Dice.
    :type _dice_sides: int
    :param _add: The number of Dice to add to the shaker.
    :type _add: int
    :param _expected_min: The expected min number the shaker can roll.
    :type _expected_min: int
    :param _expected_max: The expected max number the shaker can roll.
    :type _expected_max: int
    """
    # Create a new Dn sided Dice to use with the shaker
    _dice = dice.Dice(_dice_sides)
    # Create the new shaker to test
    shaker = dice.DiceShaker()
    # Add the dice to the shaker
    shaker.add(_dice, _add)
    # Roll the dice in the shaker
    total, rolled = shaker.roll()
    assert total == shaker.roll_total
    assert shaker.roll_total == sum(rolled)
    # Make sure the roll total is with the expected min/max bounds
    assert shaker.roll_total >= _expected_min
    assert shaker.roll_total <= _expected_max


def test_dice_module_main(capsys):
    """Test

    Check invoking the main dice module.

    :param capsys: The capsys Fixture
    :type capsys: function
    """
    # Check that the main function returns nothing, with or without args
    assert dice.main() is None
    # Capture the output
    captured = capsys.readouterr()
    # TODO Verify the output contains what we expect
    # assert "..." in captured.out
    # Verify the stderr stream is empty
    assert captured.err == ""

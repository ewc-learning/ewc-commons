"""EWC Commons Dice Rolls Tests

Test the happy & unhappy paths of creating and using the module.

TODO Test the unhappy paths.
"""
# Import pytest for testing
import pytest

# Import the typing library for type casting
from typing import Callable

# Import library stuff
from ewccommons import str_lower_list

# Import the dice module to test
from ewccommons import dice
from ewccommons import dicerolls


@pytest.mark.parametrize(
    "_dice, _expected_die_faces",
    [
        (dicerolls.D3, [*range(1, 3 + 1)]),
        (dicerolls.D4, [*range(1, 4 + 1)]),
        (dicerolls.D6, [*range(1, 6 + 1)]),
        (dicerolls.D8, [*range(1, 8 + 1)]),
        (dicerolls.D10, [*range(1, 10 + 1)]),
        (dicerolls.D12, [*range(1, 12 + 1)]),
        (dicerolls.D20, [*range(1, 20 + 1)]),
    ],
)
def test_dice_size(_dice: dice.Dice, _expected_die_faces: dice._Die_Faces_):
    """Happy Test

    Check the created Dn Dice has the expected number of side faces.

    :param _dice: The Dice instance to test.
    :type _dice: :class:`ewccommons.dice.Dice`
    :param _expected_die_faces: The die faces expected.
    :type _expected_die_faces: `ewccommons.dice._Die_Faces_`
    """
    # Verify the dice length with the expected faces length
    assert len(_dice) == len(_expected_die_faces)
    # Verify the dice faces match the expected faces
    assert _dice.faces == _expected_die_faces


@pytest.mark.parametrize(
    "_dice, _expected_max",
    [
        (dicerolls.D3, 3),
        (dicerolls.D4, 4),
        (dicerolls.D6, 6),
        (dicerolls.D8, 8),
        (dicerolls.D10, 10),
        (dicerolls.D12, 12),
        (dicerolls.D20, 20),
    ],
)
def test_dice_rolls(_dice: dice.Dice, _expected_max: int):
    """Happy Test

    Check rolling the dice is within expected paramaters.

    :param _dice: (dice.Dice) The Dice instance to test.
    :type _dice: :class:`ewccommons.dice.Dice`
    :param _expected_max: The expected max number the dice instance can roll.
    :type _expected_max: int
    """
    # Get the die roll for checking
    roll = _dice.roll()
    # Verify it's an int
    assert isinstance(roll, int)
    # Veryify the roll matches the rolled
    assert roll == _dice.rolled
    # Verify it's greater than 1
    assert roll >= 1
    # Verify it's less than the expected max
    assert roll <= _expected_max


@pytest.mark.parametrize(
    "_func, _expected_max",
    [
        (dicerolls.roll_d3, 3),
        (dicerolls.roll_d4, 4),
        (dicerolls.roll_d6, 6),
        (dicerolls.roll_d8, 8),
        (dicerolls.roll_d10, 10),
        (dicerolls.roll_d12, 12),
        (dicerolls.roll_d20, 20),
        (dicerolls.roll_d100, 100),
    ],
)
def test_rolls(_func: Callable[[], int], _expected_max: int):
    """Happy Test

    Check rolling the dice does what it should.

    :param _func: The `roll_d[n]` convenience function to test.
    :type _func: Callable[[], int]
    :param _expected_max: The max number the function can return as a roll.
    :type _expected_max: int
    """
    # Get the die roll for checking
    roll = _func()
    # Verify it's an int
    assert isinstance(roll, int)
    # Verify it's greater than 1
    assert roll >= 1
    # Verify it's less than the expected max
    assert roll <= _expected_max


def test_dicerolls_module_main(capsys):
    """Test

    Check invoking the main dice module.

    :param capsys: The capsys Fixture
    :type capsys: function
    """
    # Check that the main function returns nothing, with or without args
    assert dicerolls.rolls() is None
    # Simulate the module main being invoked by converting the input list
    # This is what happens to the arguments list in module main function
    # Calling the rolls() function with the lower case converted argument list
    cli_die_rolls = str_lower_list(["d3", "d6", "D4", "D100"])
    # Make sure the module can still handle multiple dice rolls
    assert dicerolls.rolls(*cli_die_rolls) is None
    # Capture the output
    captured = capsys.readouterr()
    # Verify the output contains what we expect
    # We do expect all dice arguments to roll & generate output
    assert "D3 Rolls..." in captured.out
    assert "D4 Rolls..." in captured.out
    assert "D6 Rolls..." in captured.out
    assert "D100 Rolls..." in captured.out
    # Verify the stderr stream is empty
    assert captured.err == ""

"""EWC Commons Card Deck Tests

Test the happy & unhappy paths of creating and using the module.
"""
# Import pytest for testing & the carddeck module to test
import pytest
from ewccommons import carddeck


def test_new_deck_size():
    """Happy Test

    Check a new deck contains the right amount of cards.
    """
    # Get a new deck & check the length matches a standard deck size
    deck: carddeck._Deck_ = carddeck.new_deck()
    assert len(deck) == carddeck.STANDARD_DECK_SIZE


def test_shuffled_deck_size():
    """Happy Test

    Check a new deck contains the right number of cards after it's shuffled.
    """
    # Get a new deck & shuffle it
    deck: carddeck._Deck_ = carddeck.new_deck()
    shuffled_deck: carddeck._Deck_ = carddeck.shuffle_deck(deck)
    # Check the shuffled length matches the original deck length
    # & a standard deck size
    assert len(shuffled_deck) == len(deck)
    assert len(shuffled_deck) == carddeck.STANDARD_DECK_SIZE


def test_new_shuffled_deck_size():
    """Happy Test

    Check a new shuffled deck contains the right number of cards
    """
    # Get a new shuffled deck & check the length matches a standard deck size
    shuffled_deck: carddeck._Deck_ = carddeck.new_shuffled_deck()
    assert len(shuffled_deck) == carddeck.STANDARD_DECK_SIZE


@pytest.mark.parametrize(
    "_draw",
    [
        (0),
        (3),
        (5),
        (12),
    ],
)
def test_draw_card_sizes(_draw):
    """Happy Test

    Check that the number of cards drawn and remaining deck cards are correct.

    :param _draw: A valid number of cards to draw.
    :type _draw: int
    """
    # Define the variable types used
    drawn: carddeck._Hand_
    deck_remaining: carddeck._Deck_
    # Define the number of cards to draw
    # Draw the cards from a new deck
    drawn, deck_remaining = carddeck.draw_card(deck=carddeck.new_deck(), cards=_draw)
    assert len(drawn) == _draw
    # Check that the remaining number of cards in the deck is the same as the
    # standard deck size minus the number of drawn cards
    assert len(deck_remaining) == carddeck.STANDARD_DECK_SIZE - _draw
    # Check that the number of cards drawn is the same as the expected
    assert len(drawn) == _draw


@pytest.mark.parametrize(
    "_draw",
    [
        (-1),
        (False,),
        (carddeck.STANDARD_DECK_SIZE + 1),
        ("hmmm"),
    ],
)
def test_invalid_draw_card_sizes(_draw):
    """Unhappy Test

    Check that invalid draw numbers respond how we expect.

    :param _draw: An invalid value for the number of cards to draw.
    :type _draw: Any
    """
    with pytest.raises(ValueError):
        # Try to draw invalid number of cards from a new deck
        carddeck.draw_card(deck=carddeck.new_deck(), cards=_draw)


def test_carddeck_module_main(capsys):
    """Test

    Check invoking the main dice module.

    :param capsys: The capsys Fixture
    :type capsys: function
    """
    # Check that the main function returns nothing, with or without args
    assert carddeck.spread_deck() is None
    assert carddeck.spread_deck(5) is None
    # Capture the output
    captured = capsys.readouterr()
    # TODO Verify the output contains what we expect
    # assert "..." in captured.out
    # Verify the stderr stream is empty
    assert captured.err == ""

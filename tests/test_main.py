"""EWC Commons Main Library Tests

Test the happy & unhappy paths of creating and using the module.
"""
# Import pytest for testing & the main module to test
import pytest
import ewccommons

# Import the typing library so variables can be type cast
from typing import Any, List


class MockDroneProcessor(ewccommons.DroneProcessor):
    """Test / Mock Class Stub (whatever)

    Create an extension to check the drone processor instance

    :param payload: The payload to process.
    :type payload: Any
    """

    def __init__(self, payload: Any) -> None:
        """Initialize the Mock Processor

        Set the instance properties.
        """
        super().__init__(payload=payload)

    def process(self):
        """Abstraction Implementation

        Pro-cessing...
        """
        # Erm, just set the payload as processed, it's only a test of the framework
        self.processed(_result=(self.processing_payload,))


@pytest.mark.parametrize(
    "_name, _version, _contains",
    [
        (None, None, ewccommons._app_name_),
        (None, None, ewccommons.__version__),
        ("!test!", None, "!test!"),
        (None, "0.0.1a", "0.0.1a"),
        (None, 0.1, "0.1"),
        ("", "", ewccommons._app_name_),
        ("", "", ewccommons.__version__),
    ],
)
def test_module_app_version(_name, _version, _contains):
    """Happy Test

    Check app version function returns as expected.

    :param _name: A app name param value to test with.
    :type _name: str
    :param _version: A version number value to test with.
    :type _version: str
    :param _contains: A text string the app_version call should contain.
    :type _contains: str
    """
    # Check that the app version contains the specified text
    assert _contains in ewccommons.generate_app_version_string(
        name=_name, version=_version
    )


@pytest.mark.parametrize(
    "source_list, expected_list",
    [
        (["lower", "UPPER", "MiXeD"], ["lower", "upper", "mixed"]),
        ([1, "", True], ["1", "", "true"]),
        ([-1, "NuMb3r5", False], ["-1", "numb3r5", "false"]),
    ],
)
def test_str_lower_list(source_list, expected_list):
    """Happy Test

    Check that the string list lower case convertor helper function works.

    :param source_list: A source list of strings to convert to lower case.
    :type source_list: List[Any]
    :param expected_list: The expected lower case list contents.
    :type expected_list: List[str]
    """
    # Convert the list & make sure it checks out as expected
    lower_list: List[str] = ewccommons.str_lower_list(source_list=source_list)
    assert lower_list == expected_list


def test_drone_process():
    """Happy Test

    Check that DroneProcessor is still an abstract base.
    """
    _payload = "None - TypeError: 'NoneType' object is not iterable"
    # Create an Mock DroneProcessor object
    processor: ewccommons.DroneProcessor = MockDroneProcessor(payload=_payload)
    # Verify it's an DroneProcessor
    assert isinstance(processor, ewccommons.DroneProcessor)
    # Make sure the processed result is empty & without error
    assert processor.processed_result is None
    assert not processor.has_error
    assert processor.error is None
    # Make sure it's the same payload to process
    assert processor.processing_payload == _payload
    # Process the payload, i.e. move it to the processed result
    processor.process()
    # Verify the result is as expected, i.e. a tuple of just the payload
    assert processor.processed_result == (_payload,)


def test_abstract_drone_process():
    """Unhappy Test

    Check that DroneProcessor is still an abstract base.
    """
    with pytest.raises(TypeError):
        # Try to create an abstract DroneProcessor object
        ewccommons.DroneProcessor(None)

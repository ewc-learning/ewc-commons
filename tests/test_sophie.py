"""EWC Commons Sophie Tests

Test the happy & unhappy paths of creating and using the module.
"""
# Import pytest for testing
import pytest

# Import the typing library so variables can be type cast
from typing import Tuple, Any, List

# Import sophie module to test
from ewccommons import sophie
from ewccommons.sophie.usage import CLIUsageHandler
from ewccommons.sophie.launcher import wrapped_launch_handler, SophieLauncher


def launch_option_handler(
    opt: str,
    arg: str,
    opts: Tuple,
    args: Tuple,
) -> Tuple[Any, ...]:
    """
    Just handler the launch option
    """
    pass


def test_get_sophie_launcher():
    """Happy Test

    Check that sophie launcher works.
    """
    # Get a new sophie launcher to work with
    t_sophie, usage = sophie.get_launcher(_usage=CLIUsageHandler)
    # Verify the return types from the launher
    assert isinstance(t_sophie, SophieLauncher)
    assert isinstance(usage, CLIUsageHandler)
    # Make sure no errors
    assert not t_sophie.has_error
    # Check the app name & version
    assert t_sophie.app_name == sophie._app_name_
    assert t_sophie.app_version == sophie._app_version_


def test_sophie_launcher_invalid_config():
    """Unhappy Test

    Check that sophie launcher fails correctly without Exception/Error.
    """
    # Get a new sophie launcher to work with
    t_sophie, usage = sophie.get_launcher(_usage=CLIUsageHandler)
    # Verify there are no errors, yes we have no errors ;)
    # Well no, but actually yes!
    assert not t_sophie.has_error
    # `pytest --cov ewccommons` supplies the invalid --cov to the argv list
    # So can't just use sys.argv[1:] because it may or may not pass
    # Depending on the --cov flag
    t_sophie.config_launch(["--invalid"])
    assert t_sophie.has_error


@pytest.mark.skip(reason="need to Mock the sys.exit() behaviour invoked")
def test_sophie_launcher_show_version(capsys):
    """Happy Test

    Check that sophie launcher shows the version.

    :param capsys: Fixture
    """
    # Get a new sophie launcher to work with
    t_sophie, usage = sophie.get_launcher(_usage=CLIUsageHandler)
    t_sophie.config_launch(["--version"])
    # Capture the output
    captured = capsys.readouterr()
    # TODO Mock the sys.exit() behaviour invoked
    # Define the usage & types of variables to be used in this scope
    enquiry: str
    handler_results: List[Tuple[Any, ...]]
    other: Tuple
    # enquiry, args = sophie.launch(cli_argument_handler)
    enquiry, handler_results, other = t_sophie.launch(
        wrapped_launch_handler(launch_option_handler)
    )
    # Verify the output contains what we expect
    assert t_sophie._app_name_ in captured.out
    assert t_sophie._app_version_ in captured.out
    # Verify the stderr stream is empty
    assert captured.err == ""


@pytest.mark.skip(reason="need to Mock the sys.exit() behaviour invoked")
def test_sophie_launcher_show_help(capsys):
    """Happy Test

    Check that sophie launcher shows the version.

    :param capsys: Fixture
    """
    # Get a new sophie launcher to work with
    t_sophie, usage = sophie.get_launcher(_usage=CLIUsageHandler)
    t_sophie.config_launch(["--help"])
    # TODO Mock the sys.exit() behaviour invoked
    # Define the usage & types of variables to be used in this scope
    enquiry: str
    handler_results: List[Tuple[Any, ...]]
    other: Tuple
    # enquiry, args = sophie.launch(cli_argument_handler)
    enquiry, handler_results, other = t_sophie.launch(
        wrapped_launch_handler(launch_option_handler)
    )
    # Capture the output
    captured = capsys.readouterr()
    # Verify the output contains what we expect
    assert sophie._app_name_ in captured.out
    assert usage.help() in captured.out
    # Verify the stderr stream is empty
    assert captured.err == ""

ewccommons package
==================

Subpackages
-----------

.. toctree::

    ewccommons.sophie

Submodules
----------

ewccommons.carddeck module
--------------------------

.. automodule:: ewccommons.carddeck
    :members:
    :undoc-members:
    :show-inheritance:

ewccommons.dice module
----------------------

.. automodule:: ewccommons.dice
    :members:
    :undoc-members:
    :show-inheritance:

ewccommons.dicerolls module
---------------------------

.. automodule:: ewccommons.dicerolls
    :members:
    :undoc-members:
    :show-inheritance:

ewccommons.errors module
------------------------

.. automodule:: ewccommons.errors
    :members:
    :undoc-members:
    :show-inheritance:

ewccommons.typing\_extensions module
------------------------------------

.. automodule:: ewccommons.typing_extensions
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: ewccommons
    :members:
    :undoc-members:
    :show-inheritance:

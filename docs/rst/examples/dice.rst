Dice Module
-----------

The **dice** module provides an OOP solution for creating various sided
die simulators and a conveniant way of rolling multiple (varying) Dice
objects

.. _dice_example:

.. code-block:: python
    :linenos:

    from ewccommons.dice import Dice, DiceShaker

    # Set a number of sides for the dice
    number_of_die_sides: int = 4
    # Create a n sided, named Dice object
    dice: Dice = Dice(number_of_die_sides, name="My New Die", val=None)
    # Have a look at what is created
    print(
        f"Dice({number_of_die_sides})",
        dice,
        dice < number_of_die_sides,
        d > 0,
        sep="\n",
    )
    # Roll the dice and get the rolled value
    dice_roll: int = dice.roll()
    # Get the last rolled value
    dice_rolled: int = dice.rolled
    # The dice object can be compared like an int, using the rolled value
    if dice == number_of_die_sides:
        # The dice object can be converted to string 
        # This gives a readable version of the last rolled value
        print("Highest Roll", dice, sep="\n")
    # The dice object also supports new object copy
    # The copy will have the same number of sides/faces and dice name
    # The copy will also have the starting value of the last rolled
    dice2: Dice = dice.copy()
    # Roll both dice to hopefully get 2 different numbers 
    # It is possible for both objects to independantly choose the same random value
    dice.roll()
    dice2.roll()
    # Verify the 2 dice could be different in value 
    print(dice, dice2, dice.rolled, dice2.rolled, sep="\n")

    # TODO Add the DiceShaker example code

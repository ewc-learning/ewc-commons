Sophie Module
-------------

The **sophie** module is used to provide convenient CLI script launcher
framework

.. _sophie_example:

.. code-block:: python

   # Import the typing library so variables can be type cast
   from typing import List

   # Import the sys for args
   import sys

   # Import the Sophie launcher system needed
   from ewccommons import sophie

   def cli_argument_handler(
       opts: List,
       args: List,
       show_usage_help: sophie._TNGN_,
       show_version: sophie._TNGN_,
   ) -> sophie._CLI_Arg_Handled_:
       """Define a function handle the processing of cli arguments"""
       my_arg: str = None
       opt: str
       arg: str
       for opt, arg in opts:
           if opt in ("-h", "--help"):
               # Just show the usage help & halt
               show_version()
               show_usage_help()
               sys.exit()
           elif opt in ("-v", "--version"):
               # Just show the version & halt
               show_version()
               sys.exit()
           elif opt == "--my_arg":
               # Assign the my_arg value
               my_arg = arg
       return my_arg, args

   def help_text() -> str:
       """Define a function to generate the help text to display"""
       return "\n".join(
           [
               "Dynamically invoked help from show_usage_help() call",
               "  -h --help (Show this help)",
               "  -v --version (Show version)",
               "  --my_arg <my-arg-value>",
           ])

   # With most of the setup done above, let's start creating the launcher
   # Define the returned object variables for use
   my_app: sophie.SophieLauncher = None
   usage: sophie.CLIUsageHandler = None
   # Create a new launcher based on the settings provided
   my_app, usage = launcher(
       # This doesn't need to be an instance of the usage handler
       # Just a reference to a variable that matches the CLIUsageHandler
       # The handler is created internally based of the settings provided
       _usage=sophie.CLIUsageHandler,
       # Name the script
       app_name="My New Script",
       # Set the script version, defaults to the version of the sophie launcher 
       version="0.0.1",
       # Set any short CLI argument option names.
       # These are a string of single character values for the option names
       # Characters immediately followed with a : (colon) will expect a value
       # E.G. f: would expect -f=<value> or -f <value>
       # Let's just use the inbuilt functionality to show help & version
       short_options="hv:",
       # Set any long CLI argument option names
       # Options with immediately followed with a = (equals) will expect a value
       # E.G. my_arg= would expect -my_arg=<value> or -my_arg <value>
       # Let's include the inbuilt functionality to show help & version
       # Let's also add a option of our own that expects user supplied value
       long_options=["help", "version", "my_arg="],
       # This is the script help text 
       # Displayed when show_usage_help() is invoked from sophie._CLI_Arg_Handler_
       # Acessible as a string through the returned CLIUsageHandler usage object
       # Also accessible a string via SophieLauncher with app name & version included
       # N.B.
       # This doesn't have to be a callback as long as it can be converted to string
       help_text=help_text,
   )
   # Example of the help_text option in use
   # my_app.show_usage_help()
   # usage.help()

   # Let's have a look at what we got
   print(
       "Let's have a look:",
       "#############",
       "my_app",
       my_app,
       "usage",
       usage,
       "#############",
       sep="\n",
   )
   # Define the usage & types of variables returned from the launch
   my_arg: str
   # Launch the script using the cli_argument_handler function to process the CLI options
   my_arg, args = my_app.launch(cli_argument_handler)
   # Lets have a look at what came back
   print(my_arg, args, sep="\n")

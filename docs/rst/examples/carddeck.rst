Card Deck Module
----------------

The **carddeck** module provides a convenient means of creating card deck
list which can be used as a basis for card based games.

.. _carddeck_example:

.. code-block:: python

   from ewccommons.carddeck import (
       _Deck_,
       _Hand_,
       shuffle_deck,
       draw_card,
       new_deck,
       new_shuffled_deck,
   )
           
   deck: _Deck_ = new_deck()
   shuffled_deck: _Deck_ = shuffle_deck(deck)
   # alternitively create a new shuffled deck
   shuffled_deck_alt: _Deck_ = new_shuffled_deck()

   hand_size:int = 5
   drawn: _Hand_
   deck_remaining:_Deck_
   drawn, deck_remaining = draw_card(deck=shuffled_deck, cards=hand_size)

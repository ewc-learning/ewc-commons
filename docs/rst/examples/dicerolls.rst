Dice Rolls Module
-----------------

The **dicerolls** module can be used in multiple ways, firstly it can be
used in the standard way of being imported into python scripts.
**dicerolls** can also be invoked as a module script and will output the
dice rolls for the dice specified. The same dice rolling functionality
can also be invoked via the main **ewccommons** package executable.

.. _dicerolls_cli_example:

.. code-block:: bash

   $ python -m ewccommons.dicerolls d3 d4 d6 D8 D10 d12 d20 D100
   ...
   $ ewccommons --dicerolls "d3 d4 d6 D8 D10 d12 d20 D100"
   ...

.. _dicerolls_code_example:

.. code-block:: python

   from ewccommons.dicerolls import roll_d6, D6

   # Impliment the functional approach to rolling a standard Dn sided die
   roll:int = roll_d6()
   # Returns a new D6 die roll
   # Impliment the OOP approach to rolling a standard Dn sided die
   dice_roll:int = D6.roll()
   # Returns the last rolled value of OOP Dn Dice instance
   dice_rolled:int = D6.rolled()

.. ewc-commons documentation master file, created by
   sphinx-quickstart on Tue Nov 16 05:38:51 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ewc-commons's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README
   rst/index
   rst/modules
   rst/examples/sophie
   rst/examples/dice
   rst/examples/dicerolls
   rst/examples/carddeck

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Code Examples
=============

* :doc:`rst/examples/sophie`
* :doc:`rst/examples/dice`
* :doc:`rst/examples/dicerolls`
* :doc:`rst/examples/carddeck`

.. note::

   This project is under active development.